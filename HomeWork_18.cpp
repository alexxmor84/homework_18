#include <iostream>

class Stack
{
	int size = 5;
	int lastIndex = 0;
	int* arr = new int[size];

public:
	Stack() : size()
	{}

	Stack(int _size) : size(_size)
	{}
	
	void push(int value)
	{
		if (lastIndex >= size)
		{
			int* tmp_ptr = new int[size * 2]; //�������� ����� ������� ������ � ��� ���� ������

			for (int i = 0; i < size; i++) //�������� ����� ����������� ��������
			{
				tmp_ptr[i] = arr[i];
			}
			delete[] arr; //������� ������ ������� ������

			arr = tmp_ptr; //��������� ���������, ����� �� �������� �� ����� ������� ������

			size = size * 2; // ��������� ������� ������ ���������� ������
		}
	
		arr[lastIndex] = value;

		lastIndex++; // ����������� �� 1
	}

	int pop()
	{
		

		if (lastIndex > 0)
		{	
			lastIndex--;
			return arr[lastIndex];

		}

		std::cout << "Stack is empty";
		return 0;
	}

	void Show()
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << arr[i] << " ";
		}
		std::cout << std::endl;
	}

	~Stack() { delete[] arr; arr = nullptr;}
};

int main()
{
	Stack st(5);
	st.Show();
	st.push(80);
	st.push(81);
	st.push(82);
	st.Show();
	st.pop();
	st.Show();
	
	
}
